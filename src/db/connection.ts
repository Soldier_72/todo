import * as mysql from "mysql";
import * as dotenv from "dotenv";

var result = dotenv.config({
     path: __dirname + '/../../.env'
})

if (result.error) throw result.error

export const mySqlConnection = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE
})